/*===========================================================================
  Copyright (C) 2012-2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.filters.its;

import org.w3c.dom.Node;

/**
 * Holds the information for the Target Pointer data category on a given node.
 */
class TargetPointerEntry {

	static final String SRC_TRGPTRFLAGNAME = "\u10ff"; // Name of the user-data property that holds the target pointer flag in the source
	static final String TRG_TRGPTRFLAGNAME = "\u20ff"; // Name of the user-data property that holds the target pointer flag in the target

	static final int BEFORE = 0;
	static final int AFTER = 1;
		
	private boolean translate;
	private Node srcNode;
	private Node trgNode;
	private boolean hasExistingTargetContent;

	/**
	 * Creates a {@link #TargetPointerEntry} object.
	 * @param srcNode the source node.
	 * @param trgNode the target node.
	 */
	TargetPointerEntry (Node srcNode,
		Node trgNode)
	{
		this.srcNode = srcNode;
		this.trgNode = trgNode;
	}

	/**
	 * Gets the source node for this entry.
	 * @return the source node for this entry.
	 */
	Node getSourceNode () {
		return srcNode;
	}
	
	/**
	 * Gets the target node for this entry.
	 * @return the target node for this entry.
	 */
	Node getTargetNode () {
		return trgNode;
	}

	/**
	 * Sets the flag indicating if the source node of this pair is translatable. 
	 * @param translate true if it is to be translated, false otherwise.
	 */
	void setTranslate (boolean translate) {
		this.translate = translate;
	}
	
	/**
	 * Gets the flag indicating if the source node of this pair is translatable.
	 * @return true if it to be translated, false otherwise.
	 */
	boolean getTranslate () {
		return this.translate;
	}
	
	/**
	 * sets the flag indicating if there is an initial target content for the pair.
	 * @param hasExistingTargetContent true if there is an initial target content, false otherwise.
	 */
	void setHasExistingTargetContent (boolean hasExistingTargetContent) {
		this.hasExistingTargetContent = hasExistingTargetContent;
	}
}
