package net.sf.okapi.filters.openxml;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PushbackReader;
import java.io.Reader;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;

import net.sf.okapi.common.Util;
import net.sf.okapi.common.exceptions.OkapiBadFilterInputException;

/**
 * Wrapper around a regular ZipFile to provide additional
 * functionality.
 */
public class OpenXMLZipFile {
	private ZipFile zipFile;
	private String encoding;
	private ContentTypes contentTypes;
	private XMLInputFactory inputFactory;
	private XMLOutputFactory outputFactory;
	private XMLEventFactory eventFactory;
	private String mainDocumentPart;

	public static final String CONTENT_TYPES_PART = "[Content_Types].xml";
	public static final String ROOT_RELS_PART = "_rels/.rels";

	private static final String OFFICE_DOCUMENT_RELATIONSHIP =
			Namespaces.DocumentRels.getDerivedURI("/officeDocument");

	// Encoding is passed in for legacy reasons, it might be
	// better to determine it ourselve
	public OpenXMLZipFile(ZipFile zipFile, XMLInputFactory inputFactory, XMLOutputFactory outputFactory,
						  XMLEventFactory eventFactory, String encoding) {
		this.zipFile = zipFile;
		this.inputFactory = inputFactory;
		this.outputFactory = outputFactory;
		this.eventFactory = eventFactory;
		this.encoding = encoding;
	}

	private void initializeContentTypes() throws XMLStreamException, IOException {
		contentTypes = new ContentTypes(inputFactory);
		contentTypes.parseFromXML(getPartReader(CONTENT_TYPES_PART));
	}

	public XMLFactories getFactories() {
		return new XMLFactories() {
			@Override
			public XMLOutputFactory getOutputFactory() {
				return outputFactory;
			}
			@Override
			public XMLInputFactory getInputFactory() {
				return inputFactory;
			}
			@Override
			public XMLEventFactory getEventFactory() {
				return eventFactory;
			}
		};
	}

	/**
	 * Determine the main part from the officeDocument relationship in the root
	 * rels file, then use its content type to figure out what kind of document
	 * this is.
	 */
	public DocumentType createDocument(ConditionalParameters params) throws XMLStreamException, IOException {
		initializeContentTypes();
		mainDocumentPart = findMainDocumentPart();
		switch (contentTypes.getContentType(mainDocumentPart)) {
		case ContentTypes.Types.Word.MAIN_DOCUMENT_TYPE:
			return new WordDocument(this, params);
		case ContentTypes.Types.Excel.MAIN_DOCUMENT_TYPE:
			return new ExcelDocument(this, params);
		case ContentTypes.Types.Powerpoint.MAIN_DOCUMENT_TYPE:
			return new PowerpointDocument(this, params);
		}
		return null;
	}

	public ContentTypes getContentTypes() {
		return contentTypes;
	}

	public XMLInputFactory getInputFactory() {
		return inputFactory;
	}

	public XMLOutputFactory getOutputFactory() {
		return outputFactory;
	}

	public XMLEventFactory getEventFactory() {
		return eventFactory;
	}

	public String getMainDocumentPart() throws IOException, XMLStreamException {
		return mainDocumentPart;
	}

	private String findMainDocumentPart() throws IOException, XMLStreamException {
		Relationships rels = getRelationships(ROOT_RELS_PART);
		Relationships.Rel docRel = rels.getRelByType(OFFICE_DOCUMENT_RELATIONSHIP).get(0);
		if (docRel == null) {
			return null;
		}
		return docRel.target;
	}

	/**
	 * Return a reader for the named document part. The encoding passed to
	 * the constructor will be used to decode the content.  Bad things will
	 * happen if you call this on a binary part.
	 * @param partName name of the part. Should not contain a leading '/'.
	 * @return Reader
	 * @throws IOException
	 */
	public Reader getPartReader(String partName) throws IOException {
		ZipEntry entry = zipFile.getEntry(partName);
		if (entry == null) {
			throw new OkapiBadFilterInputException("File is missing " + partName);
		}
		// OpenXML documents produced by Office generally don't include BOMs, but
		// they may appear in documents produced by other sources
		return Util.skipBOM(new InputStreamReader(zipFile.getInputStream(entry), encoding));
	}

	/**
	 * Parse the named document part as a relationships file and return the parsed
	 * relationships data.
	 * @param partName name of the part. Should not contain a leading '/'.
	 * @return {@link Relationships} instance
	 * @throws IOException
	 * @throws XMLStreamException
	 */
	public Relationships getRelationships(String relsPartName) throws IOException, XMLStreamException {
		Relationships rels = new Relationships(inputFactory);
		rels.parseFromXML(relsPartName, getPartReader(relsPartName));
		return rels;
	}

	/**
	 * Find the relationships file for the named part and then parse the relationships.
	 * If no relationships file exists for the specified part, an empty Relationships
	 * object is returned.
	 * @param partName
	 * @return
	 * @throws IOException
	 * @throws XMLStreamException
	 */
	public Relationships getRelationshipsForPart(String partName) throws IOException, XMLStreamException {
		int lastSlash = partName.lastIndexOf("/");
		if (lastSlash == -1) {
			return getRelationships("_rels/" + partName + ".rels");
		}
		String relPart = partName.substring(0, lastSlash) + "/_rels" + partName.substring(lastSlash) + ".rels";
		return getRelationships(relPart);
	}

	public InputStream getInputStream(ZipEntry entry) throws IOException {
		return zipFile.getInputStream(entry);
	}

	public ZipFile getZip() {
		return zipFile;
	}

	public void close() throws IOException {
		zipFile.close();
	}

	public Enumeration<? extends ZipEntry> entries() {
		return zipFile.entries();
	}
}
