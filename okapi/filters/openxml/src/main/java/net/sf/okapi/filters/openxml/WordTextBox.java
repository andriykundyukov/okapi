package net.sf.okapi.filters.openxml;

import java.util.ArrayList;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.EventType;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.filters.PropertyTextUnitPlaceholder.PlaceholderAccessType;
import net.sf.okapi.common.resource.RawDocument;

public class WordTextBox {

	private OpenXMLContentFilter textBoxOpenXMLContentFilter;

	/**
	 * Constructor for {@link Property} only. All offsets are the same, useful
	 * for creating placeholders for read-only {@link Property}s
	 * 
	 * @param type
	 *            - a {@link PlaceholderAccessType}
	 * @param name
	 *            - attribute name
	 * @param value
	 *            - attribute value
	 */
	public WordTextBox(XMLFactories xmlFactories, ConditionalParameters filterParams) {
		textBoxOpenXMLContentFilter = new OpenXMLContentFilter(xmlFactories, filterParams);
	}

	// RawDocument should be closed after OpenXMLContentFilter.close()
	public void open(String txbx, LocaleId sourceLanguage)
	{
		textBoxOpenXMLContentFilter.open(
				new RawDocument((CharSequence)txbx,sourceLanguage));
	}
	public ArrayList<Event> doEvents()
	{
		Event event;
		EventType etyp;
		ArrayList<Event> textBoxEventList = new ArrayList<Event>();
		while(textBoxOpenXMLContentFilter.hasNext())
		{
			event = textBoxOpenXMLContentFilter.next();
			etyp = event.getEventType();
			if (!(etyp==EventType.START_DOCUMENT || etyp==EventType.END_DOCUMENT))
				textBoxEventList.add(event);
		}
		return textBoxEventList;
	}	
	OpenXMLContentFilter getTextBoxOpenXMLContentFilter()
	{
		return textBoxOpenXMLContentFilter;
	}
}
