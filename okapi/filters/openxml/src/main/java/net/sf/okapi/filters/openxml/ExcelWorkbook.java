package net.sf.okapi.filters.openxml;

import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

/**
 * Class to parse XLSX workbook files.
 */
public class ExcelWorkbook {
	class Sheet {
		String name;
		String id;
		String relId;
	}

	static final QName SHEET = Namespaces.SpreadsheetML.getQName("sheet");
	static final QName SHEET_NAME = new QName("name");
	static final QName SHEET_ID = new QName("sheetId");

	private List<Sheet> sheets = new ArrayList<Sheet>();

	public List<Sheet> getSheets() {
		return sheets;
	}

	void addSheet(String name, String id, String relId) {
		Sheet sheet = new Sheet();
		sheet.name = name;
		sheet.id = id;
		sheet.relId = relId;
		sheets.add(sheet);
	}
	
	static ExcelWorkbook parseFrom(XMLEventReader reader) throws XMLStreamException {
		ExcelWorkbook workbook = new ExcelWorkbook();
		while (reader.hasNext()) {
			XMLEvent event = reader.nextEvent();
			if (event.isStartElement()) {
				StartElement e = event.asStartElement();
				if (SHEET.equals(e.getName())) {
					workbook.addSheet(e.getAttributeByName(SHEET_NAME).getValue(),
									  e.getAttributeByName(SHEET_ID).getValue(),
									  e.getAttributeByName(Relationships.ATTR_REL_ID).getValue());
				}
			}
		}
		return workbook;
	}
}
