package net.sf.okapi.filters.openxml;

import static net.sf.okapi.filters.openxml.ParseType.MSWORDDOCPROPERTIES;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.TreeSet;
import java.util.zip.ZipEntry;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;

import net.sf.okapi.common.exceptions.OkapiBadFilterInputException;
import net.sf.okapi.filters.openxml.ContentTypes.Types.Common;
import net.sf.okapi.filters.openxml.ContentTypes.Types.Excel;

public class ExcelDocument extends DocumentType {
	private SharedStringMap sharedStringMap = new SharedStringMap();
	private List<String> worksheetEntryNames = null;
	private ExcelStyles styles;
	private Relationships workbookRels;

	private static final String SHARED_STRING_TABLE_REL =
			Namespaces.DocumentRels.getDerivedURI("/sharedStrings");
	private static final String STYLES_REL =
			Namespaces.DocumentRels.getDerivedURI("/styles");

	ExcelDocument(OpenXMLZipFile zipFile, ConditionalParameters params) {
		super(zipFile, params);
	}

	@Override
	public ParseType getDefaultParseType() {
		return ParseType.MSEXCEL;
	}

	@Override
	public void initialize() throws IOException, XMLStreamException {
		String mainDocumentPart = getZipFile().getMainDocumentPart();
		workbookRels = getZipFile().getRelationshipsForPart(mainDocumentPart);
		worksheetEntryNames = findWorksheets();
		styles = parseStyles();
	}

	@Override
	public OpenXMLPartHandler getHandlerForFile(ZipEntry entry,
			String contentType, boolean squishable) {

		// Check to see if this is non-translatable
		if (!isTranslatableType(entry.getName(), contentType)) {
			return new NonTranslatablePartHandler(getZipFile(), entry);
		}
		if (contentType.equals(Excel.WORKSHEET_TYPE)) {
			return new ExcelWorksheetPartHandler(getZipFile(), entry, sharedStringMap, styles,
						findWorksheetNumber(entry.getName()), getParams());
		}
		else if (contentType.equals(Excel.SHARED_STRINGS_TYPE)) {
			return new SharedStringPartHandler(getZipFile(), entry, sharedStringMap, getParams());
		}

		OpenXMLContentFilter openXMLContentFilter =
				new OpenXMLContentFilter(getZipFile().getFactories(), getParams());
		ParseType parseType = ParseType.MSEXCEL;
		if (contentType.equals(Excel.COMMENT_TYPE)) {
			parseType = ParseType.MSEXCELCOMMENT;
		}
		else if (Common.CORE_PROPERTIES_TYPE.equals(contentType)) {
			parseType = MSWORDDOCPROPERTIES;
		}

		openXMLContentFilter.setUpConfig(parseType);
		openXMLContentFilter.setPartName(entry.getName());

		return new StandardPartHandler(openXMLContentFilter, getParams(), getZipFile(), entry, false);
	}

	private boolean isTranslatableType(String entryName, String type) {
		if (!entryName.endsWith(".xml")) return false;
		if (type.equals(Excel.SHARED_STRINGS_TYPE)) return true;
		// This is kind of a lie -- these aren't translated per se, but need special processing
		if (type.equals(Excel.WORKSHEET_TYPE)) return true;
		if (getParams().getTranslateComments() && type.equals(Excel.COMMENT_TYPE)) return true;
		if (type.equals(Excel.TABLE_TYPE)) return true;
		if (getParams().getTranslateDocProperties() && type.equals(Common.CORE_PROPERTIES_TYPE)) return true;
		return false;
	}

	/**
	 * Do additional reordering of the entries for XLSX files to make
	 * sure that worksheets are parsed in order, followed by the shared
	 * strings table.
	 * @throws XMLStreamException
	 * @throws IOException
	 */
	@Override
	public Enumeration<? extends ZipEntry> getZipFileEntries() throws IOException, XMLStreamException {
		Enumeration<? extends ZipEntry> entries = getZipFile().entries();
		List<? extends ZipEntry> entryList = Collections.list(entries);
		List<String> worksheetsAndSharedStrings = new ArrayList<String>();
		worksheetsAndSharedStrings.addAll(worksheetEntryNames);
		worksheetsAndSharedStrings.add(findSharedStrings());
		Collections.sort(entryList, new ZipEntryComparator(worksheetsAndSharedStrings));
		return Collections.enumeration(entryList);
	}

	public ExcelWorkbook parseWorkbook(String partName) throws IOException, XMLStreamException {
		XMLEventReader r = getZipFile().getInputFactory().createXMLEventReader(getZipFile().getPartReader(partName));
		return ExcelWorkbook.parseFrom(r);
	}

	ExcelStyles parseStyles() throws IOException, XMLStreamException {
		Relationships.Rel stylesRel = workbookRels.getRelByType(STYLES_REL).get(0);
		ExcelStyles styles = new ExcelStyles();
		styles.parse(getZipFile().getInputFactory().createXMLEventReader(
					 getZipFile().getPartReader(stylesRel.target)));
		return styles;
	}

	/**
	 * Examine relationship information to find all worksheets in the package.
	 * Return a list of their entry names, in order.
	 * @return list of entry names.
	 * @throws XMLStreamException
	 * @throws IOException
	 */
	List<String> findWorksheets() throws IOException, XMLStreamException {
		List<String> worksheets = new ArrayList<String>();
		ExcelWorkbook workbook = parseWorkbook(getZipFile().getMainDocumentPart());
		List<ExcelWorkbook.Sheet> sheets = workbook.getSheets();
		for (ExcelWorkbook.Sheet sheet : sheets) {
			Relationships.Rel sheetRel = workbookRels.getRelById(sheet.relId);
			worksheets.add(sheetRel.target);
		}
		return worksheets;
	}

	/**
	 * Parse relationship information to find the shared strings table.  Return
	 * its entry name.
	 * @throws IOException
	 * @throws XMLStreamException
	 */
	String findSharedStrings() throws IOException, XMLStreamException {
		String mainDocumentPart = getZipFile().getMainDocumentPart();
		Relationships rels = getZipFile().getRelationshipsForPart(mainDocumentPart);
		List<Relationships.Rel> r = rels.getRelByType(SHARED_STRING_TABLE_REL);
		if (r == null || r.size() != 1) {
			throw new OkapiBadFilterInputException("Excel document has unexpected number of string tables");
		}
		return r.get(0).target;
	}

	int findWorksheetNumber(String worksheetEntryName) {
		for (int i = 0; i < worksheetEntryNames.size(); i++) {
			if (worksheetEntryName.equals(worksheetEntryNames.get(i))) {
				return i + 1; // 1-indexed
			}
		}
		throw new IllegalStateException("No worksheet entry with name " +
						worksheetEntryName + " in " + worksheetEntryNames);
	}
}
