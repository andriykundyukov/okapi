/*===========================================================================
  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.connectors.bifile;

import java.nio.charset.Charset;

import net.sf.okapi.common.ParametersDescription;
import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.uidescription.EditorDescription;
import net.sf.okapi.common.uidescription.IEditorDescriptionProvider;

public class Parameters extends StringParameters implements IEditorDescriptionProvider {

	private static final String BIFILE = "biFile";
	private static final String INPUTENCODING = "inputEncoding";

	public Parameters () {
		super();
	}

	public String getBiFile () {
		return getString(BIFILE);
	}
	
	public void setBiFile(String biFile) {
		setString(BIFILE, biFile);
	}
	
	public String getInputEncoding () {
		return getString(INPUTENCODING);
	}
	
	public void setInputEncoding(String inputEncoding) {
		setString(INPUTENCODING, inputEncoding);
	}	
	
	@Override
	public void reset () {
		super.reset();
		setBiFile("");
		setInputEncoding(Charset.defaultCharset().name());
	}

	@Override
	public ParametersDescription getParametersDescription () {
		ParametersDescription desc = new ParametersDescription(this);
		desc.add(BIFILE, "Bilingual file", "The bilingual file from which to leverage");
		desc.add(INPUTENCODING, "Input encoding", "The encoding of the bilingual file");
		return desc;
	}

	@Override
	public EditorDescription createEditorDescription (ParametersDescription paramsDesc) {
		EditorDescription desc = new EditorDescription("Bilingual File Connector Settings", true, false);
		desc.addPathInputPart(paramsDesc.get(BIFILE), "Bilingual file", false);
		desc.addTextInputPart(paramsDesc.get(INPUTENCODING));
		return desc;
	}

}
