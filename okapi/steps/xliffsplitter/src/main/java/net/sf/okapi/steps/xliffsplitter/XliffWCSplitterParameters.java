/*===========================================================================
  Copyright (C) 2010 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.steps.xliffsplitter;

import net.sf.okapi.common.EditorFor;
import net.sf.okapi.common.ParametersDescription;
import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.uidescription.EditorDescription;
import net.sf.okapi.common.uidescription.IEditorDescriptionProvider;
import net.sf.okapi.common.uidescription.SpinInputPart;

@EditorFor(XliffWCSplitterParameters.class)
public class XliffWCSplitterParameters extends StringParameters implements IEditorDescriptionProvider {

	private static final String THRESHOLD = "threshold";

	public XliffWCSplitterParameters() {
		super();
	}

	public void reset() {
		super.reset();
		setThreshold(5000);
	}

	public int getThreshold () {
		return getInteger(THRESHOLD);
	}
	
	public void setThreshold (int threshold) {
		setInteger(THRESHOLD, threshold);
	}

	@Override
	public ParametersDescription getParametersDescription() {
		ParametersDescription desc = new ParametersDescription(this);
		desc.add(THRESHOLD, "Maximum word-count per part:", null);
		return desc;
	}
	
	@Override
	public EditorDescription createEditorDescription (ParametersDescription paramsDesc) {
		EditorDescription desc = new EditorDescription("XLIFF Word-Count Splitter", true, false);
		SpinInputPart sip = desc.addSpinInputPart(paramsDesc.get(THRESHOLD));
		sip.setRange(2, 999);
		sip.setVertical(false);
		return desc;
	}

}