/*
 * ===========================================================================
 * Copyright (C) 2013 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
 * ===========================================================================
 */

package net.sf.okapi.steps.languagetool;

import net.sf.okapi.common.EditorFor;
import net.sf.okapi.common.ParametersDescription;
import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.uidescription.EditorDescription;
import net.sf.okapi.common.uidescription.IEditorDescriptionProvider;

@EditorFor(Parameters.class)
public class Parameters extends StringParameters implements IEditorDescriptionProvider {

	private static final String ENABLEFALSEFRIENDS = "enableFalseFriends";
	private static final String CHECKSOURCE = "checkSource";
	private static final String CHECKSPELLING = "checkSpelling";

	public Parameters () {
		super();
	}

	@Override
	public void reset () {
		super.reset();
		setCheckSource(true);
		setEnableFalseFriends(true);
		setCheckSpelling(true);
	}

	public boolean getCheckSource () {
		return getBoolean(CHECKSOURCE);
	}
	
	public void setCheckSource (boolean checkSource) {
		setBoolean(CHECKSOURCE, checkSource);
	}

	public boolean getEnableFalseFriends () {
		return getBoolean(ENABLEFALSEFRIENDS);
	}
	
	public void setEnableFalseFriends (boolean enableFalseFriends) {
		setBoolean(ENABLEFALSEFRIENDS, enableFalseFriends);
	}

	public boolean getCheckSpelling () {
		return getBoolean(CHECKSPELLING);
	}
	
	public void setCheckSpelling (boolean checkSpelling) {
		setBoolean(CHECKSPELLING, checkSpelling);
	}

	@Override
	public ParametersDescription getParametersDescription () {
		ParametersDescription desc = new ParametersDescription(this);
		desc.add(CHECKSOURCE, "Check also the source text (in addition to the target)", null);
		desc.add(ENABLEFALSEFRIENDS, "Check for false friends", null);
		desc.add(CHECKSPELLING, "Check for spelling mistakes", null);
		return desc;
	}

	@Override
	public EditorDescription createEditorDescription (ParametersDescription paramDesc) {
		EditorDescription desc = new EditorDescription("LanguageTool", true, false);
		desc.addCheckboxPart(paramDesc.get(CHECKSOURCE));
		desc.addCheckboxPart(paramDesc.get(ENABLEFALSEFRIENDS));
		desc.addCheckboxPart(paramDesc.get(CHECKSPELLING));
		return desc;
	}

}
