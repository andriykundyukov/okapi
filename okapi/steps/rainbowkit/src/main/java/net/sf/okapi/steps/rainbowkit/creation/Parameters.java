/*===========================================================================
  Copyright (C) 2008-2012 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.steps.rainbowkit.creation;

import java.util.List;

import net.sf.okapi.common.ListUtil;
import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.Util;

public class Parameters extends StringParameters {

	static public String SUPPORTFILE_SEP = "\t"; // Separator between two support file entries
	static public String SUPPORTFILEDEST_SEP = ";"; // Separator between the file pattern and its destination
	static public String SUPPORTFILE_SAMENAME = "<same>"; // Marker to indicate to use the same file name
	
	static final String WRITERCLASS = "writerClass"; //$NON-NLS-1$
	static final String WRITEROPTIONS = "writerOptions"; //$NON-NLS-1$
	static final String PACKAGENAME = "packageName"; //$NON-NLS-1$
	static final String PACKAGEDIRECTORY = "packageDirectory"; //$NON-NLS-1$ 
	static final String MESSAGE = "message"; //$NON-NLS-1$
	static final String OUTPUTMANIFEST = "outputManifest"; //$NON-NLS-1$
	static final String CREATEZIP = "createZip"; //$NON-NLS-1$
	static final String SENDOUTPUT = "sendOutput"; //$NON-NLS-1$
	/*List of the support files. The storage is done:
	 * origin1>destination1\torigin2>destination2\t...
	 * where origin is a path or path with pattern
	 * and destination is a directory relative to the root of the package,
	 * plus the file name, or <same> for the same filename
	 */
	static final String SUPPORTFILES = "supportFiles"; //$NON-NLS-1$
	
	public Parameters () {
		super();
	}
	
	@Override
	public void reset() {
		super.reset();
		setWriterClass("net.sf.okapi.steps.rainbowkit.xliff.XLIFFPackageWriter");
		setWriterOptions("");
		setPackageName("pack1");
		setPackageDirectory(Util.INPUT_ROOT_DIRECTORY_VAR);
		// Internal
		setSupportFiles("");
		setMessage("");
		setOuputManifest(true);
		setCreateZip(false);
		setSendOutput(false);
	}

	public String getWriterClass () {
		return getString(WRITERCLASS);
	}

	public void setWriterClass (String writerClass) {
		setString(WRITERCLASS, writerClass);
	}

	public String getWriterOptions () {
		return getGroup(WRITEROPTIONS);
	}

	public void setWriterOptions (String writerOptions) {
		setGroup(WRITEROPTIONS, writerOptions);
	}
	
	public String getMessage () {
		return getString(MESSAGE);
	}

	public void setMessage (String message) {
		setString(MESSAGE, message);
	}
	
	public String getPackageName () {
		return getString(PACKAGENAME);
	}

	public void setPackageName (String packageName) {
		setString(PACKAGENAME, packageName);
	}

	public String getPackageDirectory () {
		return getString(PACKAGEDIRECTORY);
	}

	public void setPackageDirectory (String packageDirectory) {
		setString(PACKAGEDIRECTORY, packageDirectory);
	}
	
	public String getSupportFiles () {
		return getString(SUPPORTFILES);
	}

	public void setSupportFiles (String supportFiles) {
		setString(SUPPORTFILES, supportFiles);		
	}
	
	public boolean getOutputManifest () {
		return getBoolean(OUTPUTMANIFEST);
	}

	public void setOuputManifest (boolean outputManifest) {
		setBoolean(OUTPUTMANIFEST, outputManifest);
	}

	public boolean getCreateZip () {
		return getBoolean(CREATEZIP);
	}

	public void setCreateZip(boolean createZip) {
		setBoolean(CREATEZIP, createZip);
	}

	public boolean getSendOutput () {
		return getBoolean(SENDOUTPUT);
	}

	public void setSendOutput (boolean sendOutput) {
		setBoolean(SENDOUTPUT, sendOutput);
	}

	public List<String> convertSupportFilesToList (String data) {
		return ListUtil.stringAsList(data, SUPPORTFILE_SEP);
	}
	
	public String convertSupportFilesToString (List<String> list) {
		return ListUtil.listAsString(list, SUPPORTFILE_SEP);
	}
}
