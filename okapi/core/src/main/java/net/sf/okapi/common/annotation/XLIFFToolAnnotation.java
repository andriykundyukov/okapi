/*===========================================================================
  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.common.annotation;

import java.util.Map;
import java.util.TreeMap;

import net.sf.okapi.common.Util;
import net.sf.okapi.common.resource.Property;
import net.sf.okapi.common.resource.StartSubDocument;

/**
 * Annotation containing the set of %lt;tool&gt; elements in an XLIFF %lt;file&gt;%lt;header&gt;.
 * Should be attached to a StartSubDocument Event.
 */
public class XLIFFToolAnnotation implements IAnnotation {
	private Map<String, XLIFFTool> tools = new TreeMap<String, XLIFFTool>(Util.createComparatorHandlingNullKeys(String.class));

	public void add(XLIFFTool tool, StartSubDocument startSubDoc) {
		tools.put(tool.getId(), tool);
		updateToolProperty(startSubDoc);
	}

	public XLIFFTool get(String toolId) {
		return tools.get(toolId);
	}

	/**
	 * Synchronize the value of the tool property on the StartSubDocument
	 * event with the XML representation of this annotation.
	 * @param startSubDoc 
	 */
	public void updateToolProperty(StartSubDocument startSubDoc) {
		Property toolPlaceholder = (startSubDoc.getProperty(Property.XLIFF_TOOL) == null) ?
			new Property(Property.XLIFF_TOOL, "") : startSubDoc.getProperty(Property.XLIFF_TOOL);
		toolPlaceholder.setValue(toXML());
	}

	public String toXML() {
		StringBuilder sb = new StringBuilder();
		for (XLIFFTool tool : tools.values()) {
			sb.append(tool.toXML());
		}
		return sb.toString();
	}
}
