/*===========================================================================
  Copyright (C) 2008-2010 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.common;

import net.sf.okapi.common.resource.BaseNameable;
import net.sf.okapi.common.resource.StartDocument;
import net.sf.okapi.common.resource.StartSubDocument;
import net.sf.okapi.common.skeleton.SkeletonUtil;

public class ResourceUtil {

	public static void copyProperties(BaseNameable srcResource, BaseNameable trgResource) {
		for (String propName : srcResource.getPropertyNames()) {
			trgResource.setProperty(srcResource.getProperty(propName));
		}
	}
	
	public static StartSubDocument startSubDocumentFromStartDocument(
			StartDocument sd,
			String id, 
			String parentId, 
			String name) {
		StartSubDocument ssd = new StartSubDocument(parentId, id);
		ResourceUtil.copyProperties(sd, ssd);
		ssd.setName(name);
		ISkeleton ssdSkel = sd.getSkeleton().clone();
		ssd.setSkeleton(ssdSkel);
		SkeletonUtil.changeParent(ssdSkel, sd, ssd);
		return ssd;		
	}
	
	public static StartDocument startDocumentFromStartSubDocument(
			StartSubDocument ssd,
			String id,
			String lineBreak) {
		StartDocument sd = new StartDocument(id);
		sd.setLineBreak("\n");
		sd.setSkeleton(ssd.getSkeleton().clone());
		ResourceUtil.copyProperties(ssd, sd);
		return sd;		
	}
}
