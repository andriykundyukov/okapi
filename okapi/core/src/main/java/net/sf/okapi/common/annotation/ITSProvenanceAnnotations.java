package net.sf.okapi.common.annotation;

import net.sf.okapi.common.IResource;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.StartGroup;
import net.sf.okapi.common.resource.StartSubDocument;
import net.sf.okapi.common.resource.TextContainer;

/**
 * Annotation for ITS Provenance meta-data.
 * Allows for a unique ID to associated with the ITSProvenanceAnnotations class
 * when attached as an IAnnotation in the Annotations class. This is necessary
 * for resolving standoff meta-data.
 */
public class ITSProvenanceAnnotations extends GenericAnnotations {

	public ITSProvenanceAnnotations() {}

	public static void addAnnotations (ITextUnit tu,
		ITSProvenanceAnnotations newSet)
	{
		addAnnotationsHelper(tu, newSet);
	}

	public static void addAnnotations (TextContainer tc,
		ITSProvenanceAnnotations newSet)
	{
		addAnnotationsHelper(tc, newSet);
	}

	public static void addAnnotations (StartGroup group,
		ITSProvenanceAnnotations newSet)
	{
		addAnnotationsHelper(group, newSet);
	}

	public static void addAnnotations (StartSubDocument startSubDoc,
		ITSProvenanceAnnotations newSet)
	{
		addAnnotationsHelper(startSubDoc, newSet);
	}

	private static <T extends IResource> void addAnnotationsHelper(T resource,
		ITSProvenanceAnnotations newSet)
	{
		if ( newSet != null ) {
			ITSProvenanceAnnotations current = resource.getAnnotation(ITSProvenanceAnnotations.class);
			if ( current == null ) {
				resource.setAnnotation(newSet);
			}
			else {
				current.addAll(newSet);
			}
		}
	}
}
