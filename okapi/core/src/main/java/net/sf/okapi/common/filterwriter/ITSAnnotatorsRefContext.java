/*===========================================================================
  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.common.filterwriter;

import java.util.Stack;

import javax.xml.stream.XMLStreamReader;

import net.sf.okapi.common.Namespaces;
import net.sf.okapi.common.annotation.GenericAnnotation;
import net.sf.okapi.common.annotation.GenericAnnotationType;

/**
 * Provides a set of generic functions to retrieve the ITS annotators reference information.
 */
public class ITSAnnotatorsRefContext {

	private XMLStreamReader reader;
	private Stack<String> annotatorsRef;
	
	public ITSAnnotatorsRefContext (XMLStreamReader reader) {
		this.reader = reader;
		annotatorsRef = new Stack<String>();
		annotatorsRef.push(null);
	}
	
	/**
	 * Reads and pushes the annotatorsRef information in the context stack.
	 * <p>The method looks if there is an ITS annotatorsRef attribute in the current node 
	 * of the provided reader (the node is assumed to be an element)
	 * and uses it if present. It uses the parent context otherwise. 
	 */
	public void readAndPush () {
		String val = reader.getAttributeValue(Namespaces.ITS_NS_URI, "annotatorsRef");
		if ( val != null ) {
			// Update the existing values if needed
			val = ITSContent.updateAnnotatorsRef(annotatorsRef.peek(), val);
		}
		else {
			val = annotatorsRef.peek();
		}
		annotatorsRef.push(val);
	}
	
	/**
	 * Pops the current annotatorsRef from the context stack. 
	 */
	public void pop () {
		annotatorsRef.pop();
	}

	/**
	 * Gets the current annotatorsRef string.
	 * @return the current annotatorsRef string (can be null).
	 */
	public String peek () {
		return annotatorsRef.peek();
	}
	
	/**
	 * Gets an annotation for the current annotatorsRef string.
	 * @return a new annotation for the current annotatorsRef string or null.
	 */
	public GenericAnnotation getAnnotation () {
		String tmp = annotatorsRef.peek(); 
		if ( tmp == null ) return null;
		return new GenericAnnotation(GenericAnnotationType.ANNOT,
			GenericAnnotationType.ANNOT_VALUE, tmp);
	}

	/**
	 * Gets the IRI for a given data category, for the current content.
	 * @param dataCategory the data category to look up.
	 * @return the IRI for the given data, or null.
	 */
	public String getAnnotatorRef (String dataCategory) {
		String tmp = annotatorsRef.peek();
		if ( tmp == null ) return null;
		return ITSContent.annotatorsRefToMap(tmp).get(dataCategory);
	}

}

