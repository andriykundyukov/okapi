package net.sf.okapi.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;

import org.junit.Test;

public class StreamUtilTest {

	@Test
	public void testCRC() {
		assertEquals(1018854380L, StreamUtil.calcCRC(this.getClass().getResourceAsStream("/ParamTest01.txt")));
		assertEquals(3995389683L, StreamUtil.calcCRC(this.getClass().getResourceAsStream("/safeouttest1.txt")));
		assertEquals(369693688L, StreamUtil.calcCRC(this.getClass().getResourceAsStream("/test_path1.txt")));
		assertEquals(681066369L, StreamUtil.calcCRC(this.getClass().getResourceAsStream("/test.html")));
	}
	
	@Test
	public void streamAsString() throws Exception {
	   File tmp = File.createTempFile("test", ".txt");
	   assertTrue(tmp.exists());
	   // bis is closed twice by try-with-resource and streamAsString, but this shows best practice of using
	   // BOMAwareInputStream
	   try (BOMAwareInputStream bis = new BOMAwareInputStream(new FileInputStream(tmp), "UTF-8")) {
		   StreamUtil.streamAsString(bis, bis.detectEncoding());
	   }
	   assertTrue("Can't delete " + tmp.getAbsolutePath(), tmp.delete());
	}
}
