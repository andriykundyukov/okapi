/*===========================================================================
  Copyright (C) 2008-2012 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.segmentation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.exceptions.OkapiException;

import com.ibm.icu.text.BreakIterator;
import com.ibm.icu.text.RuleBasedBreakIterator;
import com.ibm.icu.util.ULocale;

@Deprecated
public class ICURegex {
	
	static final int GC_LEXEM = 700; // !!! Update {700} in RBBI_RULES if changed		
	static final int WB_LEXEM = 200; // Defined by the RBBI word breaker rules
	static final int PH_LEXEM = 710;	
	
	private static final Pattern ICU_PATTERN = Pattern.compile(
		"\\\\b|\\\\B|\\\\d|\\\\D|\\\\G|\\\\N\\{|\\\\s|\\\\S|\\\\U|\\\\w|" +
		"\\\\W|\\\\x\\{|\\\\X|\\\\p\\{|\\\\P\\{" // All those processed here
	); 
	private static final Pattern icuPatternExtractor = 
		Pattern.compile(
			// Place parent rules first (those having smaller rules as fragments)
			"\\\\x\\{([0-9A-Fa-f]{1,6})}-\\\\x\\{([0-9A-Fa-f]{1,6})}" + // \x{hhhh} range (should go before non-range)					
			"|\\\\U[0-9A-Fa-f]{8}-\\\\U[0-9A-Fa-f]{8}" + // Uhhhhhhhh range (should go before non-range)
			"|(\\\\N|\\\\p|\\\\P)\\{.+?\\}" + // Named chars and props
			"|\\\\x\\{([0-9A-Fa-f]{1,6})}" + // \x{hhhh}, \x{hhhhhh}			
			"|\\\\U[0-9A-Fa-f]{8}" + // Uhhhhhhhh								
			""     
			);
	
	private static final Pattern gcPatternExtractor =
			Pattern.compile("\\\\X");
	
	private static final Pattern wbPatternExtractor =
			Pattern.compile("\\\\b|\\\\B");
	
	
	private RuleBasedBreakIterator wbIterator;
	private Map <LocaleId, RuleBasedBreakIterator> wbIterators;
	private Map <String, RuleBasedBreakIterator> phIterators;
	private List<RuleInfo> rules;
	private Map<String, Placeholder> placeholders; // pattern + Placeholder
	private String[] placeholderLookup;
	private Map<CompiledRule, RuleInfo> ruleInfoLookup; 
	private LocaleId language; // for word boundaries
	private boolean dirty;
	private Pattern phPattern; // to determine whether a rule has placeholders
	private String lastProcessedText;
	private boolean hasGraphemClusterPh;
	private boolean hasWordBoundaryPh;
	private boolean hasICURules;
	private Placeholder graphemeCluster;
	private List<Integer> boundaries;

	public ICURegex() {		
		wbIterators = new TreeMap <LocaleId, RuleBasedBreakIterator>();
		phIterators = new TreeMap <String, RuleBasedBreakIterator>();
		rules = new LinkedList<RuleInfo>(); // Order should be guaranteed
		placeholders = new LinkedHashMap<String, Placeholder>(); // Order should be guaranteed
		boundaries = new ArrayList<Integer>();
		ruleInfoLookup = new HashMap<CompiledRule, RuleInfo>(); 
		reset();
	}
	
	public void reset() {
		rules.clear();
		placeholders.clear();
		graphemeCluster = Placeholder.createGraphemeCluster(GC_LEXEM);
		setLanguage(null);
		boundaries.clear();
		ruleInfoLookup.clear();
		dirty = true;
		lastProcessedText = null;
		hasGraphemClusterPh = false;
		hasWordBoundaryPh = false;
		hasICURules = false;
	}

	public void setHasICURules(boolean hasICURules) {
		this.hasICURules = hasICURules;
	}
	
	public static boolean isICURule(String rule) {
		return ICU_PATTERN.matcher(rule).find(); 
	}
	
	protected void setLanguage (LocaleId language) {
		dirty = this.language != language;
		this.language = language;
	}
	
	public String processRule(String rule) {
		// Collect ICU patterns, replace with placeholders 
		Matcher m = icuPatternExtractor.matcher(rule);
		while (m.find()) {
			// [\\N{DIGIT ZERO}], brackets around are needed for RBBI
			String pattern = String.format("[%s]", m.group());
			Placeholder ph = placeholders.get(pattern);
			if (ph == null) {
				RuleBasedBreakIterator phIterator;
				if (phIterators.containsKey(pattern)) {
					phIterator = phIterators.get(pattern);
				}
				else {
					phIterator = Placeholder.createPhIterator(pattern, PH_LEXEM);
					phIterators.put(pattern, phIterator);
				}
				ph = new Placeholder(placeholders.size(), phIterator, PH_LEXEM);
				placeholders.put(pattern, ph);
			}
			rule = rule.replace(m.group(), ph.toString());			
			dirty = true;
		}
		
		hasGraphemClusterPh |= 
				gcPatternExtractor.matcher(rule).find();
		
		rule = rule.replace("\b", "\\b"); // Least likely backspace is meant in a rule
		hasWordBoundaryPh |= 
				wbPatternExtractor.matcher(rule).find();
		
		rule = rule.replace("\\b", Character.toString(Placeholder.WORD_BOUNDARY));
		rule = rule.replace("\\B", Character.toString(Placeholder.WORD_NON_BOUNDARY));
		rule = rule.replace("\\X", Character.toString(Placeholder.GRAPHEME_CLUSTER));
		rule = rule.replace("\\d", "\\p{Nd}");
		rule = rule.replace("\\D", "\\P{Nd}");
		rule = rule.replace("\\w", "[\\p{Ll}\\p{Lu}\\p{Lt}\\p{Lo}\\p{Nd}]");
		rule = rule.replace("\\W", "[^\\p{Ll}\\p{Lu}\\p{Lt}\\p{Lo}\\p{Nd}]");
		rule = rule.replace("\\s", "[\\t\\n\\f\\r\\p{Z}\u200B]");
		rule = rule.replace("\\S", "[^\\t\\n\\f\\r\\p{Z}\u200B]");
		return rule;
	}

	private boolean containsPlaceholder(String rule) {
		return phPattern != null && phPattern.matcher(rule).find();
	}

	public void processText(String codedText, List<CompiledRule> compRules) {
		if (!hasICURules) return;
		if (dirty) {
			if (hasWordBoundaryPh) {
				// Get a word boundary iterator from cache or create a new one
				if (wbIterators.containsKey(language)) {
					wbIterator = wbIterators.get(language);
				}
				else {
					wbIterator = (RuleBasedBreakIterator)BreakIterator.getWordInstance(
						ULocale.createCanonical(language.toString()));
					RuleBasedBreakIterator.registerInstance(wbIterator, ULocale.createCanonical(language.toString()),
							BreakIterator.KIND_WORD);
					wbIterators.put(language, wbIterator);
				}
			}
			
			// Remember rules with placeholders (next time the rules list won't 
			// have placeholders in the rules)
			// Some placeholders (e.g. word boundary-related) are resolved once here 
			for (int i = 0; i < compRules.size(); i++) {
				CompiledRule compRule = compRules.get(i);
				String rule = compRule.pattern.pattern();								
				rules.add(new RuleInfo(rule));
			}
			
			// Compile placeholder pattern
			if (placeholders.size() > 0) {
				phPattern = placeholders.size() == 1 ?
						Pattern.compile(String.format("[\\u%04X]", 
						(int) Placeholder.BASE))
						:
						Pattern.compile(String.format("[\\u%04X-\\u%04X]", 
								(int) Placeholder.BASE,
								(int) Placeholder.BASE + placeholders.size() - 1))
						;
			}
			
			placeholderLookup = 
					placeholders.keySet().toArray(new String[placeholders.size()]);			
			dirty = false;			
		}
		
		if (Util.isEmpty(codedText)) return;		
		if (codedText.equals(lastProcessedText)) return;
				
		boundaries.clear();
		int start = 0;
		int end = 0;		

		// Word boundary placeholders are extra characters inserted in
		// the analyzed text before and after the occurrence like parenthesis around an expression.
		// They are placeholders for zero-width boundaries, and not replacements for actual characters.
		//if (hasWordBoundaryPh) {
		if (hasWordBoundaryPh) {
			start = 0;
			end = 0;
			
			wbIterator.setText(codedText);
			start = wbIterator.first();
			end = start;
			
			for(;;) {
				end = wbIterator.next();
				if (end == BreakIterator.DONE) break;
				if (start >= end) break;
				
				int areaId = wbIterator.getRuleStatus();
				if (areaId == WB_LEXEM) {				
					boundaries.add(start);
					boundaries.add(end);
				}				
				start = end;
			}
		}
		
		// Collect characters matching patterns 
		
		if (hasGraphemClusterPh) {
			graphemeCluster.processText(codedText);
		}		
		for (Placeholder ph : placeholders.values()) {
			ph.processText(codedText);
		}
		
		// Adapt and recompile rules for the new text
		if (rules.size() != compRules.size()) {
			throw new OkapiException("Internal rules desynchronized");
		}
		
		ruleInfoLookup.clear();
		for (int i = 0; i < rules.size(); i++) {
			RuleInfo ruleInfo = rules.get(i);
			String rule = ruleInfo.getRule();
			
			// If the rule contains placeholders, replace them with 
			// sets of matching characters in the input
			if (hasGraphemClusterPh) {
				//String chars = charSetToString(gcChars);
				String chars = graphemeCluster.getChars();
				//String ph = Character.toString(GRAPHEME_CLUSTER);
				if (Util.isEmpty(chars)) {
					// No characters are found for the placeholder,
					// do nothing, leave the placeholder in the rule,
					// otherwise Regex fires an error at empty set 
					// String.format("\\u%04X", (int) gcInfo.toString().charAt(0))
				}
				else {
					rule = rule.replace(graphemeCluster.toString(),	String.format("[%s]+", 
							chars)); // Set of all characters found for the ph
				}
			}
				
			if (containsPlaceholder(rule)) {
				for (int phIndex = 0; phIndex < placeholders.size(); phIndex++) {
					Placeholder ph = placeholders.get(placeholderLookup[phIndex]);
					String chars = ph.getChars();
					if (Util.isEmpty(chars)) {
						// No characters are found for the placeholder,
						// do nothing, leave the placeholder in the rule,
						// otherwise Regex fires an error at empty set
					}
					else {
						rule = resolvePlaceholder(rule, ruleInfo, ph, chars);
					}												
				}
			}				
						
			// Compile the new rule and place it in the list, replacing the old one
			CompiledRule oldRule = compRules.get(i);
			CompiledRule newRule = new CompiledRule(rule, oldRule.isBreak);
			compRules.set(i, newRule);
			ruleInfoLookup.put(newRule, ruleInfo);
		}		
		lastProcessedText = codedText;
	}

	private String resolvePlaceholder(String rule, RuleInfo ruleInfo,
			Placeholder ph, String chars) {		
		StringBuilder sb = new StringBuilder();
		int start = 0;
		Matcher m = ph.getPhPattern().matcher(rule);
		while(m.find()) {
			String st = ruleInfo.isSetArea(m.start()) ? chars :
				String.format("[%s]", chars);
			sb.append(rule.substring(start, m.start()));
			sb.append(st);
			start = m.end();
		}
		if (start < rule.length())
			sb.append(rule.substring(start));
		return sb.toString();
	}

	public boolean verifyPos(int pos, CompiledRule rule, Matcher matcher, List<Integer> boundaries) {
		RuleInfo info = ruleInfoLookup.get(rule);
		if (info == null) return true;
		
		return info.verifyPos(pos, matcher, boundaries);
	}

	public List<Integer> getBoundaries() {
		return boundaries;
	}
}
