/*===========================================================================
  Copyright (C) 2008-2010 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.beans.sessions;

import java.util.HashMap;
import java.util.Map;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.encoder.EncoderContext;
import net.sf.okapi.common.encoder.JSONEncoder;
import net.sf.okapi.lib.beans.v0.PersistenceMapper;
import net.sf.okapi.lib.beans.v1.OkapiBeans;
import net.sf.okapi.lib.beans.v2.OkapiBeans2;
import net.sf.okapi.lib.persistence.VersionMapper;
import net.sf.okapi.lib.persistence.json.jackson.JSONPersistenceSession;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.SerializableString;
import com.fasterxml.jackson.core.io.CharacterEscapes;
import com.fasterxml.jackson.core.io.SerializedString;

@SuppressWarnings("deprecation")
public class OkapiJsonSession extends JSONPersistenceSession {
	public OkapiJsonSession(boolean compress) {
		super(compress);
		
		// Set Okapi JSON encoder for resources
		JsonFactory jsonFactory = getJsonFactory();
		jsonFactory.setCharacterEscapes(new JSONStringEncoder());
	}
	
	private class JSONStringEncoder extends CharacterEscapes {
		private static final long serialVersionUID = 6753659638115445103L;

		JSONEncoder encoder;
		Map<Integer, SerializableString> cache;
		
		public JSONStringEncoder() {
			encoder = new JSONEncoder();
			
			// TODO when session made thread-safe, change to ConcurrentHashMap
			cache = new HashMap<Integer, SerializableString>(); 
			IParameters params = new StringParameters();
			params.setBoolean("escapeExtendedChars", true);
			encoder.setOptions(params, "UTF-8", encoder.getLineBreak());
		}
		
		@Override
		public int[] getEscapeCodesForAscii() {
			int[] res = standardAsciiEscapesForJSON();
			res[127] = -1; // escape DEL to \u007F
			return res;
		}

		@Override
		public SerializableString getEscapeSequence(int ch) {
			if (ch > 127) return null; // Don't escape UTF-8 chars
			SerializableString res = cache.get(ch);
			if (res == null) {
				res = new SerializedString(encoder.encode(ch, EncoderContext.TEXT));
				cache.put(ch, res);
			}
			return res;
		}		
	}
	
	@Override
	public void registerVersions() {
		VersionMapper.registerVersion(PersistenceMapper.class); // v0
		VersionMapper.registerVersion(OkapiBeans.class);		// v1
		VersionMapper.registerVersion(OkapiBeans2.class);		// v2
	}

	@Override
	protected Class<?> getDefItemClass() {
		return Event.class;
	}

	@Override
	protected String getDefItemLabel() {
		return "event";
	}

	@Override
	protected String getDefVersionId() {
		return OkapiBeans2.VERSION;
	}
}
