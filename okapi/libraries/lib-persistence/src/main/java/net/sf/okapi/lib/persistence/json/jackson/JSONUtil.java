/*===========================================================================
  Copyright (C) 2008-2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.persistence.json.jackson;

import java.io.IOException;

import net.sf.okapi.common.ClassUtil;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.exceptions.OkapiIOException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class JSONUtil {

	private static ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS);
	
	/**
	 * Serialize a given object to a JSON string. 
	 * Object type information is stored in the string.
	 * @param obj the given object.
	 * @return a JSON string containing the object type info and serialized object.
	 */
	public static <T> String toJSON(T obj) {
		return toJSON(obj, false);
	}
	
	/**
	 * Serialize a given object to a JSON string. 
	 * Object type information is stored in the string.
	 * @param obj the given object.
	 * @param prettyPrint true to output the JSON string as multi-line indented text. 
	 * @return a JSON string containing the object type info and serialized object.
	 */
	public static <T> String toJSON(T obj, boolean prettyPrint) {
		JSONBean<T> bean = new JSONBean<T>();
		bean.setClassName(ClassUtil.getQualifiedClassName(obj));
		try {			
			if (prettyPrint) {
				mapper.enable(SerializationFeature.INDENT_OUTPUT);
			}
			else {
				mapper.disable(SerializationFeature.INDENT_OUTPUT);
			}
			bean.setContent(obj);
			return Util.normalizeNewlines(mapper.writeValueAsString(bean));
		} catch (JsonProcessingException e) {
			throw new OkapiIOException(e);
		}			
	}
	
	/**
	 * Deserialize an object from a given JSON string based on
	 * the type information stored in the string.
	 * @param json the given JSON string.
	 * @return a new object deserialized from the given string.
	 */
	public static <T> T fromJSON(String json) {
		try {
			JSONBean<T> bean = mapper.readValue(json, new TypeReference<JSONBean<T>>(){});
			@SuppressWarnings("unchecked")
			Class<T> cls = (Class<T>) Class.forName(bean.getClassName());			
			// Have Jackson do casting
			String content = mapper.writeValueAsString(bean.getContent());
			return mapper.readValue(content, cls);
		} catch (IOException | ClassNotFoundException e) {
			throw new OkapiIOException(e);
		}
	}	
}
