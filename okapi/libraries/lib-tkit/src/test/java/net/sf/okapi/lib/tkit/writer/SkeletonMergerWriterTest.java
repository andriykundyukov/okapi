/*===========================================================================
  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.tkit.writer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.ISegmenter;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.TestUtil;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.resource.IAlignedSegments;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.common.resource.Segment;
import net.sf.okapi.common.resource.TextUnit;
import net.sf.okapi.filters.html.HtmlFilter;
import net.sf.okapi.lib.segmentation.LanguageMap;
import net.sf.okapi.lib.segmentation.Rule;
import net.sf.okapi.lib.segmentation.SRXDocument;
import net.sf.okapi.lib.tkit.filter.BeanEventFilter;
import net.sf.okapi.lib.tkit.merge.SkeletonMergerWriter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SkeletonMergerWriterTest {

	private HtmlFilter htmlFilter;
	private String root;
	private LocaleId locEN = LocaleId.fromString("en");
	private LocaleId locFR = LocaleId.fromString("fr");
	private BeanEventWriter eventWriter;
	private BeanEventFilter eventReader;
	private SkeletonMergerWriter skeletonWriter;

	@Before
	public void setUp() {
		htmlFilter = new HtmlFilter();
		root = TestUtil.getParentDir(this.getClass(), "/dummy.txt");
		eventWriter = new BeanEventWriter();
		eventWriter.setOptions(locFR, null);
		Parameters p = new Parameters();
		p.setRemoveTarget(false);
		p.setMessage("Hello!");
		eventWriter.setParameters(p);
		eventReader = new BeanEventFilter();
		skeletonWriter = new SkeletonMergerWriter();
	}

	@After
	public void tearDown() {
		htmlFilter.close();
		eventWriter.close();
		eventReader.close();
		skeletonWriter.close();
	}

	@Test
	public void simpleMergeBilingual() {
		String input = "simple.html";

		// skeleton merger options
		skeletonWriter.setOptions(locFR, "UTF-8");
		skeletonWriter.setOutput(root + input + ".out");
		IParameters p = skeletonWriter.getParameters();
		((net.sf.okapi.lib.tkit.merge.Parameters) p).setSkeletonUri(Util
				.toURI(root + input + ".json"));

		// Serialize the source file
		writeEvents(FilterTestDriver.getEvents(htmlFilter,
				new RawDocument(Util.toURI(root + input), "UTF-8",
						LocaleId.FRENCH), null), root + input + ".json");

		// simulate a bilingual file as input
		for (Event e : FilterTestDriver.getEvents(htmlFilter, new RawDocument(
				Util.toURI(root + input), "UTF-8", LocaleId.FRENCH), null)) {
			if (e.isStartDocument()) {
				e.getStartDocument().setMultilingual(true);
				e.getStartDocument().setLocale(locFR);
			}
			if (e.isTextUnit()) {
				// make the TU bilingual
				e.getTextUnit().createTarget(locFR, true, TextUnit.COPY_ALL);
				e.getTextUnit().getTarget(locFR).clear();
				e.getTextUnit().getTarget(locFR).append("XXXJEHIIIXXX");
			}
			Event mergedEvent = skeletonWriter.handleEvent(e);
			if (mergedEvent.isTextUnit()) {
				assertEquals("XXXJEHIIIXXX", mergedEvent.getTextUnit()
						.getTarget(locFR).toString());
			}
		}
	}

	@Test
	public void simpleMergeMonolingual() {
		String input = "simple.html";

		// skeleton merger options
		skeletonWriter.setOptions(locFR, "UTF-8");
		skeletonWriter.setOutput(root + input + ".out");
		IParameters p = skeletonWriter.getParameters();
		((net.sf.okapi.lib.tkit.merge.Parameters) p).setSkeletonUri(Util.toURI(root + input + ".json"));

		// Serialize the source file
		writeEvents(FilterTestDriver.getEvents(htmlFilter,
				new RawDocument(Util.toURI(root + input), "UTF-8",
						LocaleId.FRENCH), null), root + input + ".json");

		// simulate a monlingual translated file as input
		for (Event e : FilterTestDriver.getEvents(htmlFilter, new RawDocument(
				Util.toURI(root + input), "UTF-8", LocaleId.FRENCH), null)) {
			if (e.isStartDocument()) {
				e.getStartDocument().setMultilingual(true);
				e.getStartDocument().setLocale(locFR);
			}
			if (e.isTextUnit()) {
				e.getTextUnit().getSource().clear();
				e.getTextUnit().getSource().append("XXXJEHIIIXXX");
			}
			Event mergedEvent = skeletonWriter.handleEvent(e);
			if (mergedEvent.isTextUnit()) {
				assertEquals("XXXJEHIIIXXX", mergedEvent.getTextUnit()
						.getSource().toString());
			}
		}
	}

	@Test
	public void simpleSegmentedMerge() {
		String input = "simple.html";

		// skeleton merger options
		skeletonWriter.setOptions(locFR, "UTF-8");
		skeletonWriter.setOutput(root + input + ".out");
		IParameters p = skeletonWriter.getParameters();
		((net.sf.okapi.lib.tkit.merge.Parameters) p).setSkeletonUri(Util
				.toURI(root + input + ".json"));

		// Serialize the source file
		writeEvents(FilterTestDriver.getEvents(htmlFilter,
				new RawDocument(Util.toURI(root + input), "UTF-8",
						LocaleId.FRENCH), null), root + input + ".json");

		// simulate a bilingual file as input
		for (Event e : FilterTestDriver.getEvents(htmlFilter, new RawDocument(
				Util.toURI(root + input), "UTF-8", LocaleId.FRENCH), null)) {
			if (e.isStartDocument()) {
				e.getStartDocument().setMultilingual(true);
			}
			if (e.isTextUnit()) {
				e.getTextUnit().createSourceSegmentation(
						createSegmenterWithRules(locEN));
				// make the TU bilingual and copy source segments
				e.getTextUnit().createTarget(locFR, true, TextUnit.COPY_ALL);
			}
			Event mergedEvent = skeletonWriter.handleEvent(e);
			if (mergedEvent.isTextUnit()) {
				assertTrue(mergedEvent.getTextUnit().getTarget(locFR)
						.hasBeenSegmented());
				IAlignedSegments segments = mergedEvent.getTextUnit()
						.getAlignedSegments();
				Segment src = segments.getSource(0, locFR);
				Segment trg = segments.getCorrespondingTarget(src, locFR);
				assertEquals(src.text.toString(), trg.text.toString());
			}
		}
	}

	private void writeEvents(List<Event> events, String path) {
		// Serialize all the events
		eventWriter.setOutput(path);
		for (Event event : events) {
			eventWriter.handleEvent(event);
		}
		eventWriter.close();
	}

	private ISegmenter createSegmenterWithRules(LocaleId locId) {
		SRXDocument doc = new SRXDocument();
		LanguageMap langMap = new LanguageMap(".*", "default");
		doc.addLanguageMap(langMap);
		// Add the rules
		ArrayList<Rule> langRules = new ArrayList<Rule>();
		langRules.add(new Rule("\\.", "\\s", true));
		// Add the rules to the document
		doc.addLanguageRule("default", langRules);
		// Create the segmenter
		return doc.compileLanguageRules(locId, null);
	}
}
